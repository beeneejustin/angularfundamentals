//ZONE PALLETE LIST
app.controller('ZonePalleteCtrl', ['$scope', 'ZoneService', '$state', '$rootScope', '$ionicModal', 'LoadingService', function ($scope, ZoneService, $state, $rootScope, $ionicModal, LoadingService) {

    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.pickid = $rootScope.zonePickId;
        $scope.getZonePalleteList();
        $scope.userid = $rootScope.userid;
        $scope.pickDetail = {};
        $scope.filter = {};
        $scope.filter.lot = true;
    }


    //Functions

    //for modal
    $ionicModal.fromTemplateUrl('templates/moreInfoModal.html', {
        id: 'moreInfoModal',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.moreInfoModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/pickItemModal.html', {
        id: 'pickItemModal',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.pickItemModal = modal;
    });

    $scope.showMoreInfoModal = function (palnum) {
        try {
            $scope.moreInfoModal.show();
            $scope.pickedItem = _.findWhere($scope.PalleteList, { 'PalleteNo': palnum });
            console.log($scope.pickedItem);
        }
        catch (err) {
            console.log(err.message);
        }
    }

    $scope.showPickItemModal = function (palnum) {
        try {
            $scope.pickDetail.lot = "";
            $scope.pickDetail.qty = "";
            $scope.pickItemModal.show();
            $scope.pickedItem = _.findWhere($scope.PalleteList, { 'PalleteNo': palnum });
            console.log($scope.pickedItem);
        }
        catch (Err) {
            console.log(err.message);
        }
    }

    //close modal
    $scope.closeModal = function (modal) {
        if (modal == 'PickModal') {
            $scope.pickItemModal.hide();
        }
        else if (modal == 'MoreInfo') {
            $scope.moreInfoModal.hide();
        }
    }


    $scope.getZonePalleteList = function () {
        ZoneService.getZonePalleteList()
            .then(function (res) {
                $scope.PalleteList = ZoneService.ZonePalleteList;
                console.log($scope.PalleteList);
                LoadingService.StopLoading();
                if ($scope.PalleteList.length === 0) {
                    $state.go('app.zonepicklist');
                }
            }, function (err) {
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pallete-list of Picklist '" + $scope.pickid + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).finally(function () {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });
    }

    $scope.PickAll = function (palnum) {
        LoadingService.StartLoading();
        ZoneService.PickItem(palnum, '', '')
            .then(function (res) {
                console.log('Successfully Stored Proc item');
                $scope.getZonePalleteList();
                LoadingService.StopLoading();
                LoadingService.PopAlert('Item Picked', 'The item is picked');
            }, function (err) {
                LoadingService.StopLoading();
            })
    }

    $scope.PickItem = function (palnum) {
        LoadingService.StartLoading();
        ZoneService.PickItem(palnum, $scope.pickDetail.lot, $scope.pickDetail.qty)
            .then(function () {
                console.log('Sucessfully tirggered stored proc');
                $scope.pickItemModal.hide();
                $scope.getZonePalleteList();
                LoadingService.StopLoading();
                LoadingService.PopAlert('Item Picked', 'The item is picked');
            }, function (err) {
                LoadingService.StopLoading();
            })
    }


    $scope.init();

}])
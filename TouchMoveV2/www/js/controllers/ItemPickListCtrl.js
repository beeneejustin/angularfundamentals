//Item PickList Controller
app.controller('ItemCodePickListCtrl', ['$scope', 'ItemService', '$ionicLoading', 'LoadingService', '$rootScope', '$state', function ($scope, ItemService, $ionicLoading, LoadingService, $rootScope, $state) {
    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.getItemPickList();
        $scope.ItemCode = $rootScope.itemCode;

    }

    $scope.getItemPickList = function () {
        ItemService.getItemPickList()
            .then(function (res) {
                $scope.ItemPickList = ItemService.ItemPickList;
                console.log(res);
                LoadingService.StopLoading();

                if ($scope.ItemPickList.length === 0) {
                    $state.go('app.itemlist');
                }
            }, function (err) {
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pick-list of Item '" + $scope.ItemCode + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).
            finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            })



    }

    $scope.redirectMe = function (pickid) {
        $rootScope.ItemPickId = pickid;
        $state.go('app.itemcodepalletelist');
    }

    $scope.init();

}]);


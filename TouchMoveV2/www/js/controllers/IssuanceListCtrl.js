//Issuance List Controller
app.controller('IssuanceListCtrl', ['$scope', 'IssuanceService', '$state', '$rootScope', 'LoadingService', function ($scope, IssuanceService, $state, $rootScope, LoadingService) {

    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.getIssuanceList();
        $scope.wc = IssuanceService.userProfile.w;
    }

    $scope.getIssuanceList = function () {
        IssuanceService.getIssuanceList()
            .then(function (res) {
                //success
                $scope.issuanceList = IssuanceService.IssuanceList;
                LoadingService.StopLoading();
            }, function (err) {
                //err
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the IssuanceNo-list of Warehouse '" + $scope.wc + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).
            finally(function () {
                //stop ion refresher
                $scope.$broadcast('scroll.refreshComplete');
            })
    };

    $scope.redirectMe = function (issNo) {
        $rootScope.issuanceNo = issNo;
        console.log($rootScope.issuanceNo);
        $state.go('app.issuancepicklist');
    };

    $scope.init();

}]);

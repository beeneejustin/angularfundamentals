//View Controller
app.controller('ViewPalleteCtrl', ['$scope', 'LoginService', 'LoadingService', '$http', '$rootScope', function ($scope, LoginService, LoadingService, $http, $rootScope) {


    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.PickId = $rootScope.viewPickId
        $scope.getUser();
        $scope.getPallete();
    }

    $scope.getUser = function () {
        $scope.userid = LoginService.getProfile().u;
        $scope.wc = LoginService.getProfile().w;
    }

    $scope.getPallete = function () {
        try {

            $http.get($rootScope.url + 'getViewPalleteList?userid=' + $scope.userid + '&pickid=' + $scope.PickId)
                .then(function (res) {
                    $scope.PickedItems = res.data;
                    console.log(res);
                    LoadingService.StopLoading();
                }, function (data) {
                    LoadingService.StopLoading();
                    LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pallete-list of Picklist '" + $scope.PickId + "'. Please try again later<br><br>Code : " + data.status + "<br> Status : " + data.statusText);
                }).finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                })

        }
        catch (err) {
            console.log('Failed Geeting Item');
            LoadingService.StopLoading();
        }

    }

    $scope.init();

}])
//Issuance PickList Controller
app.controller('IssuancePickListCtrl', ['$scope', 'IssuanceService', '$ionicLoading', 'LoadingService', '$rootScope', '$state', function ($scope, IssuanceService, $ionicLoading, LoadingService, $rootScope, $state) {
    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.getIssuancePickList();
        $scope.IssuanceNo = $rootScope.issuanceNo;

    }

    $scope.getIssuancePickList = function () {
        IssuanceService.getIssuancePickList()
            .then(function (res) {
                $scope.IssuancePickList = IssuanceService.IssuancePickList;
                console.log(res);
                LoadingService.StopLoading();

                if ($scope.IssuancePickList.length === 0) {
                    $state.go('app.itemlist');
                }
            }, function (err) {
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pick-list of Issuance '" + $scope.IssuanceNo + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).
            finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            })



    }

    $scope.redirectMe = function (pickid) {
        $rootScope.IssuancePickId = pickid;
        $state.go('app.issuancepalletelist');
    }

    $scope.init();

}]);


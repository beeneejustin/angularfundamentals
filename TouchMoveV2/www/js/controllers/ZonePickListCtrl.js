
///ZONE PICKLIST CONTROLLER
app.controller('ZonePickListCtrl', ['$scope', 'ZoneService', '$state', '$rootScope', '$ionicHistory', 'LoadingService', function ($scope, ZoneService, $state, $rootScope, $ionicHistory, LoadingService) {
    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.zone = $rootScope.zone;
        $scope.getZonePickList();
    }


    $scope.getZonePickList = function () {
        ZoneService.getZonePickList()
            .then(function (res) {
                $scope.zonePickList = ZoneService.ZonePickList;
                console.log($scope.zonePickList);
                LoadingService.StopLoading();
                if (zonePickList.length === 0) {
                    $state.go('app.zonelist');
                }
            }, function (err) {
                //error  
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pick-list of Zone '" + $scope.zone + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).finally(function () {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });
    }

    $scope.redirectMe = function (pickid) {
        $rootScope.zonePickId = pickid;
        $state.go('app.zonepalletelist')
    }

    $scope.init();
}]);


//Issuance PalleteList Controller
app.controller('IssuancePalleteListCtrl', ['$scope', 'IssuanceService', '$state', '$rootScope', '$ionicModal', 'LoadingService', function ($scope, IssuanceService, $state, $rootScope, $ionicModal, LoadingService) {
    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.pickid = $rootScope.IssuancePickId;
        $scope.getIssuancePalleteList();
        $scope.pickDetail = {};
        $scope.filter = {};
        $scope.filter.lot = true;
    }


    //for modal
    $ionicModal.fromTemplateUrl('templates/moreInfoModal.html', {
        id: 'moreInfoModal',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.moreInfoModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/pickItemModal.html', {
        id: 'pickItemModal',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.pickItemModal = modal;
    });

    $scope.showMoreInfoModal = function (palnum) {
        try {
            $scope.moreInfoModal.show();
            $scope.pickedItem = _.findWhere($scope.PalleteList, { 'PalleteNo': palnum });
            console.log($scope.pickedItem);
        }
        catch (Err) {
            console.log(err.message);
        }
    }

    $scope.showPickItemModal = function (palnum) {
        try {
            $scope.pickDetail.qty = "";
            $scope.pickDetail.lot = "";
            $scope.pickItemModal.show();
            $scope.pickedItem = _.findWhere($scope.PalleteList, { 'PalleteNo': palnum });
            console.log($scope.pickedItem);
        }
        catch (err) {
            console.log(err.message);
        }
    }

    //close modal
    $scope.closeModal = function (modal) {
        if (modal == 'PickModal') {
            $scope.pickItemModal.hide();
        }
        else if (modal == 'MoreInfo') {
            $scope.moreInfoModal.hide();
        }
    }

    $scope.getIssuancePalleteList = function () {
        IssuanceService.getIssuancePalleteList()
            .then(function (res) {
                $scope.PalleteList = IssuanceService.IssuancePalleteList;
                console.log($scope.PalleteList);
                LoadingService.StopLoading();
                if ($scope.PalleteList.length === 0) {
                    $state.go('app.itemcodepicklist');
                }
            }, function (err) {
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pallete-list of Picklist '" + $scope.pickid + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).finally(function () {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });


    }

    $scope.PickAll = function (palnum) {
        LoadingService.StartLoading();
        IssuanceService.PickItem(palnum, '', '')
            .then(function (res) {
                console.log('Successfully Stored Proc item');
                LoadingService.PopAlert('Item Picked', 'The item is picked');
                $scope.getIssuancePalleteList();
                LoadingService.StopLoading();
            }, function (err) {
                LoadingService.StopLoading();
            })
    }

    $scope.PickItem = function (palnum) {
        LoadingService.StartLoading();
        IssuanceService.PickItem(palnum, $scope.pickDetail.lot, $scope.pickDetail.qty)
            .then(function () {
                console.log('Sucessfully tirggered stored proc');
                $scope.pickItemModal.hide();
                LoadingService.PopAlert('Item Picked', 'The item is picked');
                $scope.getIssuancePalleteList();
                LoadingService.StopLoading();
            }, function (err) {
                LoadingService.StopLoading();
            })
    }
    $scope.init();
}])
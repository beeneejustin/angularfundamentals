//ItemCode List Controller
app.controller('ItemCodeListCtrl', ['$scope', 'ItemService', '$state', '$rootScope', 'LoadingService', function ($scope, ItemService, $state, $rootScope, LoadingService) {

    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.getItemList();
        $scope.wc = ItemService.userProfile.w;
    }

    $scope.getItemList = function () {
        ItemService.getItemList()
            .then(function (res) {
                //success
                $scope.itemList = ItemService.ItemList;
                LoadingService.StopLoading();
            }, function (err) {
                //err
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the ItemCode-list of Warehouse '" + $scope.wc + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).
            finally(function () {
                //stop ion refresher
                $scope.$broadcast('scroll.refreshComplete');
            })
    };

    $scope.redirectMe = function (itmCode) {
        $rootScope.itemCode = itmCode;
        console.log($rootScope.itemCode);
        $state.go('app.itemcodepicklist');
    };

    $scope.init();

}]);

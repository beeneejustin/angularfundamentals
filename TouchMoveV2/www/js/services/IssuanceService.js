app.factory('IssuanceService', ['$http', '$q', '$rootScope', 'LoginService', function ($http, $q, $rootScope, LoginService) {

    var IssuanceService = this;

    IssuanceService.IssuanceList = {};
    IssuanceService.IssuancePickList = {};
    IssuanceService.IssuancePalleteList = {};
    IssuanceService.userProfile = {};
    IssuanceService.wc = '';

    IssuanceService.getProfile = function () {
        IssuanceService.userProfile = LoginService.getProfile();
        IssuanceService.wc = IssuanceService.userProfile.w;
        console.log(IssuanceService.userProfile);
    }

    IssuanceService.getIssuanceList = function () {
        IssuanceService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getIssuanceList?wc=' + IssuanceService.wc)
            .then(function (res) {
                IssuanceService.IssuanceList = res.data;
                defer.resolve(res);
                console.log(res);
            }, function (res) {
                console.log("error Get issuanceNo List");
                console.log(res);
                defer.reject(res);
            })

        return defer.promise;
    }

    IssuanceService.getIssuancePickList = function () {
        IssuanceService.getProfile();
        console.log('temcode' + $rootScope.issuanceNo);
        var defer = $q.defer();
        $http.get($rootScope.url + 'getIssuancePickList?wc=' + IssuanceService.wc + '&issuanceNo=' + $rootScope.issuanceNo + '')
            .then(function (res) {
                IssuanceService.IssuancePickList = res.data;
                console.log(res);
                defer.resolve(res);
            }, function (res) {
                console.log("error Get Issuance Picklist");
                console.log(res);
                defer.reject(res);
            });


        return defer.promise;
    }


    IssuanceService.getIssuancePalleteList = function () {
        IssuanceService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getIssuancePalleteList?pickid=' + $rootScope.IssuancePickId + '&issuanceNo=' + $rootScope.issuanceNo + '&wc=' + IssuanceService.wc + '')
            .then(function (res) {
                IssuanceService.IssuancePalleteList = res.data;
                console.log(res);
                defer.resolve(res);
            },function (res) {
                defer.reject(res);
            })

        return defer.promise;
    }

    IssuanceService.PickItem = function (palnum, lot, qty) {
        var defer = $q.defer();//Still thinking if will need to add if else userid exist before picking
        $http.post($rootScope.url + 'PickPalNum?palnum=' + palnum + '&lot=' + lot + '&qty=' + qty + '&userid=' + IssuanceService.userProfile.u + '')
            .then(function (res) {
                defer.resolve(res);
            },function (res) {
                defer.reject(res)
            })

        return defer.promise;
    }

    return IssuanceService;
}])
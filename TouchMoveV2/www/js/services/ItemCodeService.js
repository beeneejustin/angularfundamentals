app.factory('ItemService', ['$http', '$q', '$rootScope', 'LoginService', function ($http, $q, $rootScope, LoginService) {

    var ItemService = this;

    ItemService.ItemList = {};
    ItemService.ItemPickList = {};
    ItemService.ItemPalleteList = {};
    ItemService.userProfile = {};
    ItemService.wc = '';

    ItemService.getProfile = function () {
        ItemService.userProfile = LoginService.getProfile();
        ItemService.wc = ItemService.userProfile.w;
        console.log(ItemService.userProfile);
    }

    ItemService.getItemList = function () {
        ItemService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getItemCodeList?wc=' + ItemService.wc)
            .then(function (res) {
                ItemService.ItemList = res.data;
                defer.resolve(res);
                console.log(res);
            }, function (res) {
                console.log("error Get ItemCode List");
                console.log(res);
                defer.reject(res);
            })

        return defer.promise;
    }

    ItemService.getItemPickList = function () {
        ItemService.getProfile();
        console.log('temcode' + $rootScope.itemCode);
        var defer = $q.defer();
        $http.get($rootScope.url + 'getItemPickList?wc=' + ItemService.wc + '&ItemCode=' + $rootScope.itemCode + '')
            .then(function (res) {
                ItemService.ItemPickList = res.data;
                console.log(res);
                defer.resolve(res);
            }, function (res) {
                console.log("error Get Item Picklist");
                console.log(res);
                defer.reject(res);
            });


        return defer.promise;
    }


    ItemService.getItemPalleteList = function () {
        ItemService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getItemPalleteList?pickid=' + $rootScope.ItemPickId + '&itemCode=' + $rootScope.itemCode + '&wc=' + ItemService.wc + '')
            .then(function (res) {
                ItemService.ItemPalleteList = res.data;
                console.log(res);
                defer.resolve(res);
            },function (res) {
                defer.reject(res);
            })

        return defer.promise;
    }

    ItemService.PickItem = function (palnum, lot, qty) {
        var defer = $q.defer();//Still thinking if will need to add if else userid exist before picking
        $http.post($rootScope.url + 'PickPalNum?palnum=' + palnum + '&lot=' + lot + '&qty=' + qty + '&userid=' + ItemService.userProfile.u + '')
            .then(function (res) {
                defer.resolve(res);
            },function (res) {
                defer.reject(res)
            })

        return defer.promise;
    }

    return ItemService;
}])
//Location List Controller
app.controller('LocListCtrl', ['$scope', 'LocationService', '$state', '$rootScope', 'LoadingService', function ($scope, LocationService, $state, $rootScope, LoadingService) {

    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.getLocList();
        $scope.wc = LocationService.userProfile.w;
    }

    $scope.getLocList = function () {
        LocationService.getLocList()
            .then(function (res) {
                //success
                $scope.locList = LocationService.LocList;
                LoadingService.StopLoading();
            }, function (err) {
                //err
    LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Location-list of Warehouse '" + $scope.wc + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).
            finally(function () {
                //stop ion refresher
                $scope.$broadcast('scroll.refreshComplete');
            })
    };

    $scope.redirectMe = function (loc) {
        $rootScope.loc = loc;
        $state.go('app.locpicklist');
    };

    $scope.init();

}]);

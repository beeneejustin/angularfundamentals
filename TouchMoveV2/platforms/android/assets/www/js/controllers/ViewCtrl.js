//View Controller
app.controller('ViewPickedCtrl', ['$scope', 'LoginService', 'LoadingService', '$http', '$rootScope', '$state', function ($scope, LoginService, LoadingService, $http, $rootScope, $state) {

    $scope.init = function () {
        $scope.type = {};

        LoadingService.StartLoading();
        $scope.type.selected = '1';
        $scope.getViews();

    }

    $scope.getUser = function () {
        $scope.userid = LoginService.getProfile().u;
        $scope.wc = LoginService.getProfile().w;
        console.log($scope.userid);
    }

    $scope.getViews = function () {
        $scope.getUser();
        try {
            if ($scope.type.selected == 1) {
                $http.get($rootScope.url + 'GetLastPickedItem?userid=' + $scope.userid)
                    .then(function (res) {
                        $scope.PickedItems = [];// clear
                        if (res.data.data != null) {
                            $scope.PickedItems.push(res.data.data);
                        }
                        $scope.viewHeader = $scope.PickedItems[0].PicklistID;
                        LoadingService.StopLoading();
                    }, function (data) {
                        LoadingService.PopAlert("Something went wrong", "Sorry we can't get the last picked item. Please try again later<br><br>Code : " + data.status + "<br> Status : " + data.statusText);
                        console.log(data);
                        LoadingService.StopLoading();
                    }).finally(function () {
                        $scope.$broadcast('scroll.refreshComplete');
                    })
            }
            else if ($scope.type.selected == 2) {
                $http.get($rootScope.url + 'GetLastPickList?userid=' + $scope.userid)
                    .then(function (res) {
                        $scope.PickedItems = [];
                        $scope.PickedItems = res.data;
                        $scope.viewHeader = $scope.PickedItems[0].PicklistID;
                        LoadingService.StopLoading();
                    }, function (data) {
                        LoadingService.StopLoading();
                        LoadingService.PopAlert("Something went wrong", "Sorry we can't get the last pick-list. Please try again later<br><br>Code : " + data.status + "<br> Status : " + data.statusText);
                    }).finally(function () {
                        $scope.$broadcast('scroll.refreshComplete');
                    })
            }
            else if ($scope.type.selected == 3) {
                $http.get($rootScope.url + 'getViewPickList?userid=' + $scope.userid)
                    .then(function (res) {
                        $scope.PickListItems = res.data;
                        console.log(res);
                        $scope.viewHeader = '';
                        LoadingService.StopLoading();
                    }, function (data) {
                        LoadingService.StopLoading();
                        LoadingService.PopAlert("Something went wrong", "Sorry we can't get the pick-list. Please try again later<br><br>Code : " + data.status + "<br> Status : " + data.statusText);
                    }).finally(function () {
                        $scope.$broadcast('scroll.refreshComplete');
                    })
            }
        }
        catch (err) {
            console.log('Failed Geeting Item');
            LoadingService.StopLoading();
        }

    }

    $scope.redirect = function (pickid) {
        $rootScope.viewPickId = pickid;
        $state.go('app.viewpalletelist')
    }

    $scope.init();

}])
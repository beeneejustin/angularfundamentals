app.controller('AppCtrl', ['$scope', 'LoginService', '$rootScope', '$state', function ($scope, LoginService, $rootScope, $state) {
    $scope.init = function () {
        $scope.isLogged = false;
    }

    $scope.$on('isLoggedChecker', function (event, res) {
        $scope.isLogged = res;
        console.log('isLogged ' + $scope.isLogged);
    })


    $scope.redirectMe = function (param) {
        if (param == 'picklist') {
            $state.go('app.picklist');
        }
        else if (param == 'home') {
            $state.go('app.home');
        }
        else if (param == 'zone') {
            $state.go('app.zonelist');
        }
        else if (param == 'loc') {
            $state.go('app.loclist');
        }
        else if (param == 'view') {
            $state.go('app.viewpicked');
        }
    };

    $scope.init();
}])

app.controller('HomeCtrl', ['$scope', '$ionicModal', 'LoginService', '$ionicSideMenuDelegate', '$rootScope', '$cordovaToast', 'LoadingService', '$ionicHistory', function ($scope, $ionicModal, LoginService, $ionicSideMenuDelegate, $rootScope, $cordovaToast, LoadingService,$ionicHistory) {


    // // Form data for the login modal
    $scope.init = function () {
        $scope.loginData = {};
        $scope.dashChecker();
        $scope.warehouseList = {};
        $scope.swipeCheck();
        $scope.dateToday = new Date();
    }


    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
        var desc = _.findWhere($scope.warehouseList, { WarehouseCode: $scope.loginData.warehouse });
        LoginService.PutProfile($scope.loginData.username, $scope.loginData.password, $scope.loginData.warehouse, desc.WarehouseDescription)
            .then(function (res) {
                $scope.loginData = LoginService.userProfile;
                console.log($scope.loginData);
                $scope.broadcastLogged();
                $scope.swipeCheck();
                $scope.getDashboard();
            }, function (err) {
                //Error
            })
    };

    //HOY ADD MO DITO YUNG LOGIC NUNG SA LOSE FOCUS SEARCH WAREHOUSES
    $scope.getWarehouse = function () {

        LoginService.GetWarehouse($scope.loginData.username)
            .then(function (res) {
                $scope.warehouseList = LoginService.wcList;
                console.log($scope.warehouseList);
                if ($scope.warehouseList.length != 0) {
                    $scope.loginData.warehouse = $scope.warehouseList[0].WarehouseCode;
                    //Mas prioritize natin yung pag use ng toast para di haasle (*Note : Toast only works in mobile and emulators)
                    $cordovaToast.show('You have ' + $scope.warehouseList.length + ' warehouses available', 'short', 'bottom');
                    /* $ionicPopup.alert({
                         title: 'Warehouse Searched',
                         template: 'You have ' + $scope.warehouseList.length + ' warehouses available'
                     });*/

                }
                else if ($scope.warehouseList.length === 0) {
                    LoadingService.PopAlert('No Warehouse Available', 'You have no available warehouse, make sure you are using the right username');
                }
            }, function (err) {
                //Error
                LoadingService.PopAlert('Network Error', 'Please make sure you are connected to the internet');
            });
    };


    $scope.doLogout = function () {
        LoginService.LogOut();
        $scope.loginData = LoginService.getProfile();
        $scope.broadcastLogged();
        $scope.swipeCheck();
        $ionicHistory.clearHistory();   
        console.log($scope.loginData);//check if cleared loginData
    };

    $scope.changeWarehouse = function () {
        $scope.loginData = LoginService.changeWarehouse($scope.loginData.w);
        $scope.getDashboard();
        $scope.closeModal();
    };

    $scope.swipeCheck = function () {
        if ($scope.loginData.isLogged === true) {
            $ionicSideMenuDelegate.canDragContent(true);
        }
        else {
            $ionicSideMenuDelegate.canDragContent(false);
        }
    };

    $scope.broadcastLogged = function () {
        $rootScope.$broadcast('isLoggedChecker', $scope.loginData.isLogged);
    }

    $scope.getDashboard = function () {
        $scope.dashValue = true;
        LoginService.getDashboard($scope.loginData.w)
            .then(function () {
                $scope.DashBoardData = LoginService.DashBoardData;
                console.log("Getting DASHBOARD DATA");
                $scope.dashChecker();
            }, function () {
                console.log("Something went wrong");
            })
    }

    $scope.dashChecker = function () {
        var val = _.isEmpty($scope.DashBoardData);
        $scope.dashValue = val;
        console.log("dash val" + $scope.dashValue);
    }

    //modals
    $ionicModal.fromTemplateUrl('templates/selectWarehouse.html', {
        id: 'selectWarehouse',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.selectWarehouse = modal;
    });


    $scope.openWarehouseModal = function () {
        try {
            $scope.selectWarehouse.show();
        }
        catch (err) {
            console.log(err.message);
        }
    }

    $scope.closeModal = function () {
        $scope.selectWarehouse.hide();
    }


    $scope.init();

}])
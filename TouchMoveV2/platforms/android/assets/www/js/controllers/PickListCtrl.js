//PickList Controller
app.controller('PickListCtrl', ['$scope', 'PickListService', '$state', '$rootScope', '$ionicHistory', 'LoadingService', function ($scope, PickListService, $state, $rootScope, $ionicHistory, LoadingService) {
    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.getPickList();
        $scope.wc = PickListService.userProfile.w;
    }

    $scope.getPickList = function () {
        PickListService.getPickList()
            .then(function (res) {
                $scope.PickList = PickListService.PickList;
                console.log($scope.PickList);
                LoadingService.StopLoading();
            }, function (err) {
                //error  
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pick-list of Warehouse '" + $scope.wc + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
    }

    $scope.redirectMe = function (pickid) {
        $rootScope.pickid = pickid;
        $state.go('app.palletelist');
        console.log($rootScope.pickid);
    }

    $scope.init();
}]);

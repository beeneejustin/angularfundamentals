//ZONE LIST CONTROLLER
app.controller('ZoneListCtrl', ['$scope', 'ZoneService', '$state', '$rootScope', '$ionicHistory', 'LoadingService', function ($scope, ZoneService, $state, $rootScope, $ionicHistory, LoadingService) {

    $scope.init = function () {

        LoadingService.StartLoading();
        $scope.getZoneList();
        $scope.wc = ZoneService.userProfile.w; // for View Title so it shows Pick Zone in {{warehouse code}}

    }

    $scope.getZoneList = function () {
        ZoneService.getZoneList()
            .then(function (res) {
                //success
                $scope.zoneList = ZoneService.ZoneList;
                console.log($scope.zoneList);
                LoadingService.StopLoading();
            }, function (err) {
                //err
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Zone-list of Warehouse '" + $scope.wc + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).
            finally(function () {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });
    }

    $scope.redirectMe = function (zone) {
        $rootScope.zone = zone;
        $state.go('app.zonepicklist');
    }

    $scope.init();
}]);

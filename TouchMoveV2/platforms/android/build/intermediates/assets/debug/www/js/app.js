// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'ngCordova'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .run(function ($rootScope) {
        // $rootScope.url = 'http://localhost/TouchMoveApi/api/Picklist/';
        //$rootScope.url = 'http://150.200.46.169/TouchMoveApi/api/Picklist/';
        $rootScope.url = 'http://150.200.47.81/TouchMoveApi/api/Picklist/';

        $rootScope.zone = '';
    })

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.home', {
                url: '/home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home.html',
                        controller: 'HomeCtrl'
                    }
                }

            })

            .state('app.picklist', {
                url: '/picklist',
                cache: false,// fixed the problem of $scope.$on(Gire init() and calling it at the end of the fucntion)
                views: {
                    'menuContent': {
                        templateUrl: 'templates/picklist.html',
                        controller: 'PickListCtrl'
                    }
                }
            })

            .state('app.palletelist', {
                url: '/picklist/palletelist',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/palletelist.html',
                        controller: 'PalleteListCtrl'
                    }
                }
            })

            .state('app.zonelist', {
                url: '/zonelist',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/zonelist.html',
                        controller: 'ZoneListCtrl'
                    }
                }
            })

            .state('app.zonepicklist', {
                url: '/zonelist/picklist',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/zonepicklist.html',
                        controller: 'ZonePickListCtrl'
                    }
                }
            })

            .state('app.zonepalletelist', {
                url: '/zonelist/picklist/palletelist',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/zonepalletelist.html',
                        controller: 'ZonePalleteCtrl'
                    }
                }
            })

            .state('app.loclist', {
                url: '/loclist',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/loclist.html',
                        controller: 'LocListCtrl'
                    }
                }
            })

            .state('app.locpicklist', {
                url: '/loclist/locpicklist',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/locpicklist.html',
                        controller: 'LocPickListCtrl'
                    }
                }
            })

            .state('app.locpalletelist', {
                url: '/loclist/locpicklist/locpalletelist',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/locpalletelist.html',
                        controller: 'LocPalleteListCtrl'
                    }
                }
            })

            .state('app.viewpicked', {
                url: '/viewpicked',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/viewpicked.html',
                        controller: 'ViewPickedCtrl'
                    }
                }
            })

            .state('app.viewpalletelist', {
                url: '/viewpicked/viewpalletelist',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/viewpalletelist.html',
                        controller: 'ViewPalleteCtrl'
                    }
                }
            })


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('app/home');
    });

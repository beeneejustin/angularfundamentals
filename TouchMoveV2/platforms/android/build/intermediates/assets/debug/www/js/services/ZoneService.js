app.service('ZoneService', ['$http', '$q', '$rootScope', 'LoginService', function ($http, $q, $rootScope, LoginService) {


    var ZoneService = this;

    ZoneService.ZoneList = {};
    ZoneService.ZonePickList = {};
    ZoneService.ZonePalleteList = {};
    ZoneService.userProfile = {};
    ZoneService.wc = '';

    ZoneService.getProfile = function () {
        ZoneService.userProfile = LoginService.getProfile();
        ZoneService.wc = ZoneService.userProfile.w;
        console.log(ZoneService.userProfile);
    }


    ZoneService.getZoneList = function () {
        ZoneService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getZoneList?wc=' + ZoneService.wc)
            .then(function (res) {
                ZoneService.ZoneList = res.data;
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            })

        return defer.promise;
    }

    ZoneService.getZonePickList = function () {
        ZoneService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getZonePickList?wc=' + ZoneService.wc + '&zone=' + $rootScope.zone + '')
            .then(function (res) {
                ZoneService.ZonePickList = res.data;
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            })

        return defer.promise;
    }

    ZoneService.getZonePalleteList = function () {
        ZoneService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getZonePalleteList?pickid=' + $rootScope.zonePickId + '&zone=' + $rootScope.zone + '&wc=' + ZoneService.wc + '')
            .then(function (res) {
                ZoneService.ZonePalleteList = res.data;
                defer.resolve(res);
            }, function (err, status) {
                defer.reject(err);
            })

        return defer.promise;
    }

    ZoneService.PickItem = function (palnum, lot, qty) {
        var defer = $q.defer();//Still thinking if will need to add if else before picking
        $http.post($rootScope.url + 'PickPalNum?palnum=' + palnum + '&lot=' + lot + '&qty=' + qty + '&userid='+ ZoneService.userProfile.u+'')
            .then(function (res) {
                defer.resolve(res);
            }, function (res) {
                defer.reject(res)
            })

        return defer.promise;
    }


    return ZoneService;

}]);
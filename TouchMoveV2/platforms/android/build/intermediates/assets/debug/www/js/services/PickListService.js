app.service('PickListService', ['$http', '$q', '$rootScope', 'LoginService', function ($http, $q, $rootScope, LoginService) {

    var PickListService = this;

    PickListService.PickList = {};
    PickListService.PalleteList = {};
    PickListService.userProfile = {};
    PickListService.wc = '';

    PickListService.getProfile = function () {
        PickListService.userProfile = LoginService.getProfile();
        PickListService.wc = PickListService.userProfile.w;
        console.log(PickListService.userProfile);
    }

    PickListService.getPickList = function () {
        PickListService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getPickList?wc=' + PickListService.wc)
            .then(function (res) {
                PickListService.PickList = res.data;
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
                console.log(res);
            })
        return defer.promise;
    }

    PickListService.getPalleteList = function () {
        PickListService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getPalleteList?pickid=' + $rootScope.pickid + '&wc=' + PickListService.wc)
            .then(function (res) {
                PickListService.PalleteList = res.data;
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            })
        return defer.promise;
    }

    PickListService.PickItem = function (palnum, lot, qty) {
        PickListService.getProfile();//Still thinking if will need to add if else before picking
        var defer = $q.defer();
        $http.post($rootScope.url + 'PickPalNum?palnum=' + palnum + '&lot=' + lot + '&qty=' + qty + '&userid=' + PickListService.userProfile.u + '')
            .then(function (res) {
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            })

        return defer.promise;
    }





    return PickListService;

}])
//Location PickList Controller
app.controller('LocPickListCtrl', ['$scope', 'LocationService', '$ionicLoading', 'LoadingService', '$rootScope', '$state', function ($scope, LocationService, $ionicLoading, LoadingService, $rootScope, $state) {
    $scope.init = function () {
        LoadingService.StartLoading();
        $scope.getLocPickList();
        $scope.loc = $rootScope.loc;

    }

    $scope.getLocPickList = function () {
        LocationService.getLocPickList()
            .then(function (res) {
                $scope.locPickList = LocationService.LocPickList;
                console.log(res);
                LoadingService.StopLoading();

                if ($scope.locPickList.length === 0) {
                    $state.go('app.loclist');
                }
            }, function (err) {
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pick-list of Location '" + $scope.loc + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).
            finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            })



    }

    $scope.redirectMe = function (pickid) {
        $rootScope.locPickId = pickid;
        $state.go('app.locpalletelist');
    }

    $scope.init();

}]);


app.service('LoginService', ['$q', '$rootScope', '$http', function ($q, $rootScope, $http) {
    //Bro dont forget to make promise when you already get the API, You got this aleady
    var LoginService = this;

    LoginService.userProfile = {};
    LoginService.wcList = {};

    LoginService.PutProfile = function (user, pw, wc, wd) {
        /*Logic as for 4/18
            Shoot to Api, if not returned success, return na din from api ng isLogged = false, message = "wrong user or pass"
            if logged return ng isLogged = true and data returned and message
        */
        var defer = $q.defer();

        $http.get('http://apps.fastgroup.biz/rest/api/wms/login/?userid=' + user + '&userpass=' + pw + '')
            .success(function (res) {

                if (res.hasError == false) {
                    LoginService.wcList = _.where(res.data, { 'isSelected': true });
                    LoginService.userProfile.isLogged = true;
                    LoginService.userProfile.u = user;
                    LoginService.userProfile.w = wc;
                    LoginService.userProfile.wd = wd;

                    $rootScope.userid = LoginService.userProfile.user;
                    console.log('rs' + $rootScope.userid)
                }
                else if (res.hasError == true) {

                    LoginService.userProfile = res;
                    LoginService.userProfile.isLogged = false;
                }
                defer.resolve(res);
                console.log(LoginService.userProfile)//print userprofile
            })
            .error(function (err, status) {
                defer.reject(err);
            })

        return defer.promise;
        /*
        LoginService.userProfile = { 'u': user, 'p': pw, 'w': wc };
        LoginService.userProfile.isLogged = true;
         LoginService.userProfile.uid = user;
        //set rootscope of uid
        $rootScope.userid =  LoginService.userProfile.uid;
        console.log(LoginService.userProfile);*/
    }


    LoginService.ClearWarehouse = function () {
        console.log(LoginService.wcList.length);
        LoginService.wcList.splice(0, LoginService.wcList.length);
    }

    LoginService.GetWarehouse = function (user) {
        var defer = $q.defer();
        $http.get('http://apps.fastgroup.biz/rest/api/wms/get-user-warehouse/?userid=' + user)
            .then(function (res) {
                console.log(res.data);
                LoginService.wcList = _.where(res.data.data, { 'isSelected': true });
                defer.resolve(res);
                console.log(LoginService.wcList);
            }, function (res) {
                defer.reject(res);
                console.log(res);
                console.log("SERVICE")
            })

        return defer.promise;
    }

    LoginService.LogOut = function () {
        /*Logic RN
          username find = clear login userprofile
          Clear WC list
      */
        LoginService.userProfile = {};
        console.log(LoginService.wcList.length);
        LoginService.wcList.splice(0, LoginService.wcList.length)
        console.log("afters plice")
        console.log(LoginService.wcList);
        LoginService.userProfile.isLogged = false;
    }

    LoginService.getProfile = function () {
        return LoginService.userProfile;
    }

    LoginService.changeWarehouse = function (wc) {
        LoginService.userProfile.w = wc;
        LoginService.userProfile.wd = _.findWhere(LoginService.wcList, { WarehouseCode: wc }).WarehouseDescription;
        LoginService.getDashboard(wc);
        return LoginService.userProfile;
    }

    LoginService.getDashboard = function (warCode) {
        var defer = $q.defer();
        $http.get($rootScope.url + 'getDashboard', {
            params: { wc: warCode }
        })
            .then(function (res) {
                LoginService.DashBoardData = res.data;
                defer.resolve(res);
            }, function (res) {
                console.log(res);
                defer.reject(res);
            });
        return defer.promise;
    }


    return LoginService;

}]);

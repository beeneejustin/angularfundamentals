app.service('LoadingService', ['$ionicLoading', '$ionicPopup', function ($ionicLoading, $ionicPopup) {
    var LoadingService = this;

    LoadingService.StartLoading = function () {
        $ionicLoading.show({
            /*   animation: 'fade-in',
               content: 'Loading',
               maxWidth: 200,
               showBackdrop: true,
               showDelay: 0*/
            template: '<ion-spinner icon="ripple" class="spinner-positive"></ion-spinner>'
        });
    }

    LoadingService.StopLoading = function () {
        $ionicLoading.hide();
    }

    LoadingService.PopAlert = function (t, temp) {
        $ionicPopup.alert({
            title: t,
            template: temp
        });

    }

    return LoadingService;
}])
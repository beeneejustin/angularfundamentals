app.factory('LocationService', ['$http', '$q', '$rootScope', 'LoginService', function ($http, $q, $rootScope, LoginService) {

    var LocationService = this;

    LocationService.LocList = {};
    LocationService.LocPickList = {};
    LocationService.LocPalleteList = {};
    LocationService.userProfile = {};
    LocationService.wc = '';

    LocationService.getProfile = function () {
        LocationService.userProfile = LoginService.getProfile();
        LocationService.wc = LocationService.userProfile.w;
        console.log(LocationService.userProfile);
    }

    LocationService.getLocList = function () {
        LocationService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getLocList?wc=' + LocationService.wc)
            .then(function (res) {
                LocationService.LocList = res.data;
                defer.resolve(res);
            }, function (res) {
                console.log("error Get Location List");
                console.log(res);
                defer.reject(res);
            })

        return defer.promise;
    }

    LocationService.getLocPickList = function () {
        LocationService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getLocPickList?wc=' + LocationService.wc + '&loc=' + $rootScope.loc + '')
            .then(function (res) {
                LocationService.LocPickList = res.data;
                console.log(res);
                defer.resolve(res);
            }, function (res) {
                console.log("error Get Loc Picklist");
                console.log(res);
                defer.reject(res);
            });


        return defer.promise;
    }


    LocationService.getLocPalleteList = function () {
        LocationService.getProfile();
        var defer = $q.defer();
        $http.get($rootScope.url + 'getLocPalleteList?pickid=' + $rootScope.locPickId + '&loc=' + $rootScope.loc + '&wc=' + LocationService.wc + '')
            .then(function (res) {
                LocationService.LocPalleteList = res.data;
                console.log(res);
                defer.resolve(res);
            },function (res) {
                defer.reject(res);
            })

        return defer.promise;
    }

    LocationService.PickItem = function (palnum, lot, qty) {
        var defer = $q.defer();//Still thinking if will need to add if else userid exist before picking
        $http.post($rootScope.url + 'PickPalNum?palnum=' + palnum + '&lot=' + lot + '&qty=' + qty + '&userid=' + LocationService.userProfile.u + '')
            .then(function (res) {
                defer.resolve(res);
            },function (res) {
                defer.reject(res)
            })

        return defer.promise;
    }

    return LocationService;
}])
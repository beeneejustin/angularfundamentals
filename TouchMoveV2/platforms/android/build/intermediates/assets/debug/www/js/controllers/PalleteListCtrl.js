//Pallete Controller
app.controller('PalleteListCtrl', ['$scope', 'PickListService', '$state', '$rootScope', '$ionicModal', '$ionicHistory', 'LoadingService', function ($scope, PickListService, $state, $rootScope, $ionicModal, $ionicHistory, LoadingService) {

    $scope.init = function () {
        console.log("init");
        LoadingService.StartLoading();
        $scope.pickid = $rootScope.pickid;
        $scope.getPalleteList();
        $scope.userid = $rootScope.userid;
        $scope.pickDetail = {};
        $scope.filter = {};
        $scope.filter.lot = true;
    };



    //Functions

    //for modal
    $ionicModal.fromTemplateUrl('templates/moreInfoModal.html', {
        id: 'moreInfoModal',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.moreInfoModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/pickItemModal.html', {
        id: 'pickItemModal',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.pickItemModal = modal;
    });

    $scope.showMoreInfoModal = function (palnum) {
        try {
            console.log(palnum);
            $scope.moreInfoModal.show();
            $scope.pickedItem = _.findWhere($scope.PalleteList, { 'PalleteNo': palnum });
            console.log($scope.pickedItem);
        }
        catch (err) {
            console.log(err.message);
        }
    }

    $scope.showPickItemModal = function (palnum) {
        try {
            $scope.pickDetail.qty = "";
            $scope.pickDetail.lot = "";
            $scope.pickItemModal.show();
            $scope.pickedItem = _.findWhere($scope.PalleteList, { 'PalleteNo': palnum });
            console.log($scope.pickedItem);
        }
        catch (err) {
            console.log(err.message);
        }
    }

    //close modal
    $scope.closeModal = function (modal) {
        if (modal == 'PickModal') {
            $scope.pickItemModal.hide();
        }
        else if (modal == 'MoreInfo') {
            $scope.moreInfoModal.hide();
        }
    }

    $scope.getPalleteList = function () {
        PickListService.getPalleteList()
            .then(function (res) {
                $scope.PalleteList = PickListService.PalleteList;
                console.log($scope.PalleteList);
                LoadingService.StopLoading();
                  if ($scope.PalleteList.length === 0) {
                    $state.go('app.picklist');
                }
            }, function (err) {
                //err
                LoadingService.StopLoading();
                LoadingService.PopAlert("Something went wrong", "Sorry we cant get the Pallete-list of Picklist '" + $scope.pickid + "'. Please try again later<br><br>Code : " + err.status + "<br> Status : " + err.statusText);
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            })
    }



    $scope.PickAll = function (palnum) {
        LoadingService.StartLoading();
        PickListService.PickItem(palnum, '', '')
            .then(function (res) {
                console.log('Successfully Stored Proc item');
                LoadingService.PopAlert('Item Picked', 'The item is picked');
                $scope.getPalleteList();
                LoadingService.StopLoading();
            }, function (err) {
                LoadingService.StopLoading();
            })
    }

    $scope.PickItem = function (palnum) {
        LoadingService.StartLoading();
        if($scope.filter.lot == true){
            $scope.pickDetail.lot = '';
        }
        if($scope.filter.qty == true){
            $scope.pickDetail.qty = '';
        }
        PickListService.PickItem(palnum, $scope.pickDetail.lot, $scope.pickDetail.qty)
            .then(function () {
                console.log('Sucessfully tirggered stored proc');
                $scope.pickItemModal.hide();
                LoadingService.PopAlert('Item Picked', 'The item is picked');
                $scope.getPalleteList();
                LoadingService.StopLoading();
            }, function (err) {
                LoadingService.StopLoading();
            })
    }



    $scope.init();
}])